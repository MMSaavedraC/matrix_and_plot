from functools import reduce
import json
import matplotlib.pyplot as plt
import numpy as np
import requests


def json_to_array(json, attribute_name, attribute_dtype):
    iterator = (element[attribute_name] for element in json)
    array = np.fromiter(iterator, attribute_dtype, len(json))
    return array


def dtype_for_string_arrays(json, attribute_name):
    iterator = (element[attribute_name] for element in json)
    max_length_word = reduce(lambda element, current_max:
                         element if len(element) > len(current_max) else current_max,
                         iterator)
    datatype = np.array([max_length_word]).dtype
    return datatype


#reading the data
url = 'https://api.datos.gob.mx/v1/condiciones-atmosfericas'

response = requests.request('GET', url)
raw_json = json.loads(response.text)
raw_data = raw_json['results']

#name and tempc arrays to plot
name_dtype = dtype_for_string_arrays(raw_data, 'name')
name_array = json_to_array(raw_data, 'name', name_dtype)
tempc_array = json_to_array(raw_data, 'tempc', int)

#plotting name vs tempc
fig, ax = plt.subplots()
ypos = np.arange(len(name_array))
ax.barh(ypos, tempc_array)
ax.set_yticks(ypos)
ax.set_yticklabels(name_array, fontsize=2)
ax.invert_yaxis()
ax.set_xlabel('Temperature ºC')
plt.savefig('g.png', dpi=400)

#cityid and state arrays for the matrix
cityid_dtype = dtype_for_string_arrays(raw_data, 'cityid')
state_dtype = dtype_for_string_arrays(raw_data, 'state')
cityid_array = json_to_array(raw_data, 'cityid', cityid_dtype)
state_array = json_to_array(raw_data, 'state', state_dtype)

#obtaining and printing the matrix
joint_array = np.array([cityid_array, name_array, state_array])
result_matrix = np.asmatrix(joint_array).T
print(result_matrix)
