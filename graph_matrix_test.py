from functools import reduce
import json
import matplotlib.pyplot as plt
import numpy as np
import pytest
import requests

def json_to_array(json, attribute_name, attribute_dtype):
    iterator = (element[attribute_name] for element in json)
    array = np.fromiter(iterator, attribute_dtype, len(json))
    return array


def dtype_for_string_arrays(json, attribute_name):
    iterator = (element[attribute_name] for element in json)
    max_length_word = reduce(lambda element, current_max:
                         element if len(element) > len(current_max) else current_max,
                         iterator)
    datatype = np.array([max_length_word]).dtype
    return datatype


def test_array_creation():
    test_json = json.loads('{"results": [{"initial": "r", "number": 2}, {"initial": "q", "number": 3}]}')
    test_array = np.array([2, 3])
    assert json_to_array(test_json['results'], 'number', int).all() == test_array.all()

def test_dtype():
    test_json = json.loads('{"results": [{"init": "rest", "number": 2}, {"init": "quiet", "number": 3}]}')
    assert dtype_for_string_arrays(test_json['results'], 'init') == np.array(["quiet"]).dtype
